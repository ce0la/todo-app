// require the just installed express, bodyParser and database apps
const express = require('express');
const bodyParser = require('body-parser');

// then we call express
const app = express();
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));

// the task array with initial placeholders for added task
var task = [];
var complete = [];

// takes us to the root (/) URL and display added and completed tasks through the render command which
app.get('/', function (req, res) {
    res.render('index', {task: task, complete: complete});
});

// post route for adding new task
app.post('/addtask', function (req, res) {
    var newTask = req.body.newtask;
    
    // add the new task from the post route into the array
    task.push(newTask);
    // after adding to the array go back to the root route
    res.redirect('/');
});

//the completed task array with initial placeholders for removed task
app.post("/removetask", function (req, res) {
    var completeTask = req.body.check;
    //check for the "typeof" the different completed task, then add into the complete task
    if (typeof completeTask === "string") {
        complete.push(completeTask);
    //check if the completed task already exist in the task when checked, then remove using the array splice method
        task.splice(task.indexOf(completeTask), 1);
    } else if (typeof completeTask === "object") {
        for (var i = 0; i < completeTask.length; i++) {
            complete.push(completeTask[i]);
            task.splice(task.indexOf(completeTask[i]), 1);
        }
    }
    res.redirect("/");
});

// retaking the completed tasks back to tasks list
app.post("/readdtask", function (req, res) {
    var incompleteTask = req.body.incompleteTask;
    console.log(incompleteTask, 'hut');
    //check for the "typeof" the different completed task, then add into the complete task
    if (typeof incompleteTask === "string") {
        task.push(incompleteTask);
    //check if the completed task already exist in the task when checked, then remove using the array splice method
        complete.splice(complete.indexOf(incompleteTask), 1);
    } else if (typeof incompleteTask === "object") {
        for (var i = 0; i < incompleteTask.length; i++) {
            task.push(incompleteTask[i]);
            complete.splice(complete.indexOf(incompleteTask[i]), 1);
        }
    }
    res.redirect("/");
});

// the server is listening on port 3000 for connections
app.listen(3000, function () {
    console.log('server is on the up and up and listening on port 3000');
});